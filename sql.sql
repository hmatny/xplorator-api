-- SEQUENCE: public.city_table_id_seq

-- DROP SEQUENCE public.city_table_id_seq;

CREATE SEQUENCE public.city_table_id_seq;

ALTER SEQUENCE public.city_table_id_seq
    OWNER TO psql_master;

-- SEQUENCE: public.activity_table_id_seq

-- DROP SEQUENCE public.activity_table_id_seq;

CREATE SEQUENCE public.activity_table_id_seq;

ALTER SEQUENCE public.activity_table_id_seq
    OWNER TO psql_master;

-- SEQUENCE: public.facility_table_id_seq

-- DROP SEQUENCE public.facility_table_id_seq;

CREATE SEQUENCE public.facility_table_id_seq;

ALTER SEQUENCE public.facility_table_id_seq
    OWNER TO psql_master;

-- Table: public.city_table

-- DROP TABLE public.city_table;

CREATE TABLE public.city_table
(
    id integer NOT NULL DEFAULT nextval('city_table_id_seq'::regclass),
    city text COLLATE pg_catalog."default",
    _state text COLLATE pg_catalog."default",
    avg_income text COLLATE pg_catalog."default",
    population text COLLATE pg_catalog."default",
    num_activities integer,
    city_summary text COLLATE pg_catalog."default",
    img_url text COLLATE pg_catalog."default",
    zip text COLLATE pg_catalog."default",
    CONSTRAINT "cityKey" PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.city_table
    OWNER to psql_master;




    -- Table: public.city_activity_table

-- DROP TABLE public.city_activity_table;

CREATE TABLE public.city_activity_table
(
    city_id integer,
    activity_id integer,
    CONSTRAINT "activityForeignKey" FOREIGN KEY (activity_id)
        REFERENCES public.activity_table (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT "cityForeignKey" FOREIGN KEY (city_id)
        REFERENCES public.city_table (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.city_activity_table
    OWNER to psql_master;

    -- Table: public.facility_activity_table

-- DROP TABLE public.facility_activity_table;

CREATE TABLE public.facility_activity_table
(
    facility_id integer,
    activity_id integer,
    CONSTRAINT "activity ForeignKey" FOREIGN KEY (activity_id)
        REFERENCES public.activity_table (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT "facilityForeignKey" FOREIGN KEY (facility_id)
        REFERENCES public.facility_table (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.facility_activity_table
    OWNER to psql_master;