import json
import random
from db import Database

class ModelBuilder:
    def build_instance_data(self, model, data):
        content = {}
        content["instance-name"] = data[1]
        content["instance-description"] = data[4]
        content["img-src"] = data[2]
        content["href"] = "/" + model + "/" + str(data[0])
        return content

    def build_all(self, data, func, count=0):
        instances = []
        temp_instance = {}
        response = {}
        for row in data:
            temp_instance = {}
            key = str(row[0])
            temp_act = func(row)
            temp_instance[key] = temp_act
            instances.append(temp_instance)
        
        response["instance-data"] = instances
        response["total-count"] = count
        response = json.dumps(response)
        return response

    def build_activity_instance(self, data):
        if data == []:
            return "Error: Data passed to build_city empty"
        # activity_details stored in 1st element
        activity_details = data[0][0]
        # external cities
        ext_cities = data[1]
        # external facilities
        ext_facs = data[2]
        
        #build activity_details
        activity_content = self.build_activity_details(activity_details)

        ext_models = {}
        ext_models['Cities'] = self.build_ext(ext_cities, 'city')
        ext_models['Facilities'] = self.build_ext(ext_facs, 'facility')
        activity_content["external-models"] = ext_models
        return json.dumps(activity_content)

    def build_activity_details(self, act_data):
        act_content = {}
        act_content["instance-name"] = act_data[1]
        act_content["instance-description"] = act_data[4]
        act_content["img-src"] = act_data[2]
        href_key = "activity/" + str(act_data[0])
        act_content["href"] = href_key

        # set activity attributes
        act_attr = {}
        other_attr = {}
        disp_attr = {}

        other_attr["num-cities"] = act_data[3]
        disp_attr["num-cities"] = act_data[3]

        act_attr["displayAttributes"] = disp_attr
        act_attr["otherAttributes"] = other_attr

        act_content["attributes"] = act_attr
        return act_content



    def build_facility_instance(self, data):
        if data == []:
            return "Data passed to build_facility_instance is empty"
        #print(data)
        # facility content
        fac_details = data[0][0]
        # ext activities
        ext_acts = data[1]
        # ext city
        ext_city = data[2]

        f_content = self.build_facility_details(fac_details)
        ext_models = {}             
        ext_models['Activities'] = self.build_ext(ext_acts, 'activity')
        # only have one city/facility that is in the facility_details
        ext_models['Cities'] = {fac_details[10] : "/city/" + str(fac_details[9])}
        f_content["external-models"] = ext_models

        return json.dumps(f_content)


    def build_facility_details(self, f_data):
        f_content = {}
        f_content["instance-name"] = f_data[1]
        f_content["instance-description"] = f_data[2]
        
        if f_data[8] == "default":
            f_content["img-src"] = "https://image.flaticon.com/icons/png/512/433/433102.png"
        else:    
            f_content["img-src"] = f_data[8]


        href_key = "facility/" + str(f_data[0])
        f_content["href"] = href_key

        other_attr = {}
        disp_attr = {}
        disp_attr["Facility-Type"] = f_data[3]
        disp_attr["phone"] = f_data[5]
        disp_attr["email"] = f_data[6]
        disp_attr["Zip"] = f_data[14]

        other_attr["Directions"] = f_data[4]
        other_attr["ADA-Accessible"] = f_data[7]

        other_attr["City-Name"] = f_data[10]
        other_attr["Lattitude"] = f_data[11]
        other_attr["Longitude"] = f_data[12]
        other_attr["Cost"] = f_data[13]
        other_attr["Address"] = f_data[15]
        other_attr["phone"] = f_data[5]
        other_attr["email"] = f_data[6]
        other_attr["Zip"] = f_data[14]

        attr = {}
        attr["displayAttributes"] = disp_attr
        attr["otherAttributes"] = other_attr
        f_content["attributes"] = attr

        return f_content


    def build_city_instance(self, data):
        if data == []:
            return "Error: Data passed to build_city empty"
        # city_details stored in 1st element
        city_details = data[0][0]
        # external activities
        ext_acts = data[1]
        # external facilities
        ext_facs = data[2]
        
        #build city_details
        city_content = self.build_city_details(city_details)

        ext_models = {}        
        ext_models['Activities'] = self.build_ext(ext_acts, 'activity')
        ext_models['Facilities'] = self.build_ext(ext_facs, 'facility')
        city_content["external-models"] = ext_models
        return json.dumps(city_content)


    # given city_instance details builds appropriate json response
    def build_city_details(self, city_data):
        city_content = {}

        name = str(city_data[1]) + ", " + str(city_data[2])
        href_key = str(city_data[0])
        city_content["instance-name"] = name
        city_content["instance-description"] = city_data[6]
        city_content["img-src"] = city_data[7]
        city_content["href"] = "/city/" + href_key

        attr = {}
        disp_attr = {}
        other_attr = {}

        other_attr["Average_Income"] = city_data[3]
        other_attr["Zipcode"] = city_data[8]
        other_attr["Population"] = city_data[4]
        disp_attr["Population"] = city_data[4]
        disp_attr["Num_Activities"] = city_data[5]
        disp_attr["Average_Income"] = city_data[3]

        attr["otherAttributes"] = other_attr
        attr["displayAttributes"] = disp_attr
        city_content["attributes"] = attr
        return city_content

    # builds external models given model name and data
    def build_ext(self, ext_data, model_name):
        ext = {}
        for row in ext_data:
            name = row[1]
            href = "/" + model_name + "/" + str(row[0])
            ext[name] = href
        return ext

    # returns a dict that maps model types to build functions
    def model_func_map(self):
        dic = {}
        dic["city"] = self.build_city_details
        dic["activity"] = self.build_activity_details
        dic["facility"] = self.build_facility_details
        return dic