# hello this is my new message

# pip install flask
from flask import Flask, render_template, Response, request

# pip install -U flask-cors
from flask_cors import CORS
import json
from db import Database
from model import ModelBuilder
from flask import Response

application = Flask(__name__)
CORS(application)
database = Database()  # establish database connection
model_builder = ModelBuilder()

# gets the city data
city_data = []
city_arr = database.select_all("city")
city_data = city_arr
res_city = model_builder.build_all(city_arr, model_builder.build_city_details, len(city_arr))
city_pages=json.loads(res_city)

# gets the activity data
activity_data = []
activity_arr = database.select_all("activity")
activity_data = activity_arr
res_activity = model_builder.build_all(activity_arr, model_builder.build_activity_details, len(activity_arr))
activity_pages=json.loads(res_activity)

# gets the facility data
facility_data = []
facility_arr = database.select_all("facility")
facility_data = facility_arr
res_facility = model_builder.build_all(facility_arr, model_builder.build_facility_details, len(facility_arr))
facility_pages=json.loads(res_facility)



model_func_map = model_builder.model_func_map()

def paginate(data_arr, page_id, model):
    page_id = int(page_id)

    beg = (page_id - 1) * 6
    end = (page_id) * 6
    data_slice = data_arr[beg:end]
    result = model_builder.build_all(data_slice, model_func_map[model], len(city_arr))
    return result



def get_predicate():
    less_than = "less_than"
    greater_than = "greater_than"
    equal_to = "equal_to"
    close_to = "close_to"
    res = "None man"
    if request.args.get(less_than):
        return (less_than, request.args.get(less_than))
    elif request.args.get(greater_than):
        return (greater_than, request.args.get(greater_than))
    elif request.args.get(equal_to):
        return (equal_to, request.args.get(equal_to))
    elif request.args.get(close_to):
        return (close_to, request.args.get(close_to))
    return res

@application.route("/")
def root():

    if database.is_connected():
        return "Success connecting to database man"
    else:
        return "Something went wrong amigo"

@application.route("/search/<term>")
def search(term):
    # must search on all displayable attributes in all models 
    cities, activities, facilities = database.search_all(term)

    if (sum([len(cities),len(activities),len(facilities)]) == 0):
        return Response("{'Error':'No matches found'}", status=204, mimetype='application/json')
    
    cities = json.loads(model_builder.build_all(cities, model_builder.build_city_details))
    activities = json.loads(model_builder.build_all(activities, model_builder.build_activity_details))
    facilities = json.loads(model_builder.build_all(facilities, model_builder.build_facility_details))

    res = {}
    res["cities"] = cities
    res["activities"] = activities
    res["facilities"] = facilities
    return json.dumps(res)

@application.route("/city")
def city():
    global city_data
    return city_page(1)  # returns 1st page


# `<modelName>/filter/<attribute>?<predicate>=<value>`
#  `city/filter/numCities?lessThan=10`
@application.route("/city/filter/<attr>")
def city_filter(attr):
 
    pred_val = get_predicate()

    res = database.filter("city", attr, *pred_val)
    if len(res) == 0:
        return Response("{'Error':'No matches found'}", status=204, mimetype='application/json')
    page_id = request.args.get("page") if request.args.get("page") != None else 1
    res = paginate(res, page_id, "city")
    return res

@application.route("/city/search/<name>")
def city_search(name):
    res = database.search("city", name)
    if len(res) == 0:
        return Response("{'Error':'No matches found'}", status=204, mimetype='application/json')

    page_id = request.args.get("page") if request.args.get("page") != None else 1
    res = paginate(res, page_id, "city")
    return res

@application.route("/city/sort/<attr>")
def city_sort(attr):
    sort_by = request.args.get("sort_by")
 

    res = database.sort("city", attr, sort_by)
    for data in res:
        print(data[4])
    if len(res) == 0:
        return  Response("{'Error':'No results found'}", status=204, mimetype='application/json')
    
    page_id = request.args.get("page") if request.args.get("page") != None else 1
    res = paginate(res, page_id, "city")
    return res


@application.route("/city/page/<page_id>")
def city_page(page_id):
    result = paginate(city_arr, page_id, "city")
    return result


@application.route("/facility")
def facility():
    global facility_data
    return facility_page(1)  # returns 1st page

@application.route("/facility/page/<page_id>")
def facility_page(page_id):
    result = paginate(facility_arr, page_id, "facility")
    return result

@application.route("/facility/filter/<attr>")
def facility_filter(attr):
    pred_val = get_predicate()
    res = database.filter("facility", attr, *pred_val)
    if len(res) == 0:
        return Response("{'a':'b'}", status=204, mimetype='application/json')

    page_id = request.args.get("page") if request.args.get("page") != None else 1
    res = paginate(res, page_id, "facility")
    return res

@application.route("/facility/search/<name>")
def facility_search(name):
    res = database.search("facility", name)
    if len(res) == 0:
        return Response("{'a':'b'}", status=204, mimetype='application/json')
        
    page_id = request.args.get("page") if request.args.get("page") != None else 1
    res = paginate(res, page_id, "facility")
    return res

@application.route("/facility/sort/<attr>")
def facility_sort(attr):
    sort_by = request.args.get("sort_by")
    res = database.sort("facility", attr, sort_by)
    if len(res) == 0:
        return Response("{'a':'b'}", status=204, mimetype='application/json')
    
    page_id = request.args.get("page") if request.args.get("page") != None else 1
    res = paginate(res, page_id, "facility")
    return res

@application.route("/activity")
def activity():
    global activity_data
    return activity_page(1)  # returns 1st page

@application.route("/activity/page/<page_id>")
def activity_page(page_id):
    result = paginate(activity_arr, page_id, "activity")
    return result

@application.route("/activity/filter/<attr>")
def activity_filter(attr):
    pred_val = get_predicate()
    res = database.filter("activity", attr, *pred_val)
    if len(res) == 0:
        # TODO: change response code to signifiy error being sent
        return "Error"

    page_id = request.args.get("page") if request.args.get("page") != None else 1
    res = paginate(res, page_id, "activity")
    return res

@application.route("/activity/search/<name>")
def activity_search(name):
    res = database.search("activity", name)
    if len(res) == 0:
        return Response("{'a':'b'}", status=204, mimetype='application/json')

    page_id = request.args.get("page") if request.args.get("page") != None else 1
    res = paginate(res, page_id, "activity")
    return res

@application.route("/activity/sort/<attr>")
def activity_sort(attr):
    sort_by = request.args.get("sort_by")
    res = database.sort("activity", attr, sort_by)
    if len(res) == 0:
        return json.dumps({"Error":"No  matches found!!" })
    res = model_builder.build_all(res, model_builder.build_activity_details)
    return res

# ~~~~~~~~~~ INSTANCES ~~~~~~~~

@application.route("/all/<model>")
def select_all(model):
    res = database.select_all(model)
    res = model_builder.build_all(res, model_func_map[model])
    return res


@application.route("/city/<city_id>")
def city_id(city_id):

    # TODO: respond with error if bad param
    # TODO: the select should bring back only one result with select statment
    
    res = database.select_city(city_id)     
    if len(res[0]) == 0:
        return Response("{'Error':'CITTY DOES NOT EXIST CMON MAN'}", status=204, mimetype='application/json')

    res = model_builder.build_city_instance(res)
    return res


@application.route("/facility/<facility_id>")
def facility_id(facility_id):
    res = database.select_facility(facility_id)
    if len(res[0]) == 0:
        return Response("{'Error':'FACILITY DOES NOT EXIST CMON MAN'}", status=204, mimetype='application/json')
    result = model_builder.build_facility_instance(res)
    return result


@application.route("/activity/<activity_id>")
def activity_id(activity_id):

    res = database.select_activity(activity_id)
    if len(res[0]) == 0:
        return Response("{'Error':'ACTIVITY DOES NOT EXIST CMON MAN'}", status=204, mimetype='application/json')
    res = model_builder.build_activity_instance(res)
    return res



# run the application.
if __name__ == "__main__":
    # Setting debug to True enables debug output. This line should be
    # removed before deploying a production application.
    application.debug = True
    # application.run(host='0.0.0.0') #use this if running on aws
    application.run()  # use this if running locally
