import json
import requests
import wikipedia
import sqlalchemy
import pprint
import pickle
import argparse
import sys

import psycopg2
import os
from psycopg2 import sql
from db import Database


_table_names = { "facility_activity_table", "city_activity_table", "facility_table", "city_table", "activity_table" }


def parse_args():

	parser = argparse.ArgumentParser(description="populate the remote databases with scraped data")
	parser.add_argument('--drop', type=str, default='none', help='table to drop. Use \'all\' to drop all tables.', nargs='+')
	parser.add_argument('--create', type=str, default='none', help='table to create. Use \'all\' to create all tables.', nargs='+')
	parser.add_argument('--populate', type=str, default='all', help='which tables to populate. Use \'all\' to populate all.', nargs='+')
	parser.add_argument('--recreate', type=str, default='false', help='drops everything, re-creates everything, and repopulates everything')
	args = parser.parse_args() 
	return args

def load_everything():
	"""
	unpickles the data from API_scrape
	"""

	result = tuple()

	for n in ["t1", "t2", "t3", "t4", "t5", "t6", "t7"]:
		with open("{name}.pickle".format(name=n), "rb") as handle:
			result += ( pickle.load(handle), )
	return result



def drop_all_tables(conn, cursor):
	cursor.execute("DROP TABLE public.facility_activity_table;")
	cursor.execute("DROP TABLE public.city_activity_table;")
	cursor.execute("DROP TABLE public.facility_table;")
	cursor.execute("DROP TABLE public.city_table;")
	cursor.execute("DROP TABLE public.activity_table;")

	# cursor.execute("DROP SEQUENCE public.activity_table_id_seq;")
	# cursor.execute("DROP SEQUENCE public.city_table_id_seq;")
	# cursor.execute("DROP SEQUENCE public.facility_table_id_seq;")



# the best function to call if you want to redo db tables
def create_all_tables(conn, cursor, schemas, order):
	
	# create the sequences to manage primary key
	create_activity_seq(conn, cursor)
	create_city_seq(conn, cursor)
	create_facility_seq(conn, cursor)

	# create activity table
	create_activity_table(conn, cursor, schemas, order["activity_table"])

	# create city table
	create_city_table(conn, cursor, schemas, order["city_table"])

	# create city-activity intermediate table
	create_city_activity_table(conn, cursor)

	# create facility table
	create_facility_table(conn, cursor, schemas, order["facility_table"])

	# create facility-activity intermediate table
	create_facility_activity_table(conn, cursor)


""" CODE TO CREATE ID GENERATING SEQUENCES """

def create_city_seq(conn, cursor):
	cursor.execute("CREATE SEQUENCE public.city_table_id_seq;\n")
	cursor.execute("ALTER SEQUENCE public.city_table_id_seq OWNER TO psql_master;\n")


def create_activity_seq(conn, cursor):
	cursor.execute("CREATE SEQUENCE public.activity_table_id_seq;\n")
	cursor.execute("ALTER SEQUENCE public.activity_table_id_seq OWNER TO psql_master;\n")

def create_facility_seq(conn, cursor):
	cursor.execute("CREATE SEQUENCE public.facility_table_id_seq;\n")
	cursor.execute("ALTER SEQUENCE public.facility_table_id_seq OWNER TO psql_master;\n")

""" PRIMARY TABLE CREATION """

def create_activity_table(conn, cursor, schemas, order):
	schema = schemas["activity_table"]
	query = "CREATE TABLE public.activity_table\n(\n\tid integer NOT NULL DEFAULT nextval(\'activity_table_id_seq\'::regclass),"

	for col_name in order:
		query += "\n\t{name} {t},".format(name=col_name, t=schema[col_name])

	query += "\n\tCONSTRAINT \"idKey\" PRIMARY KEY (id)\n)\nWITH (\n\tOIDS = FALSE\n)\nTABLESPACE pg_default;\n"

	cursor.execute(query)
	cursor.execute("ALTER TABLE public.activity_table\n\tOWNER to psql_master;\n")


def create_city_table(conn, cursor, schemas, order):
	schema = schemas["city_table"]
	query = "CREATE TABLE public.city_table\n(\n\tid integer NOT NULL DEFAULT nextval(\'city_table_id_seq\'::regclass),"

	for col_name in order:
		query += "\n\t{name} {t},".format(name=col_name, t=schema[col_name])

	query += "\n\tCONSTRAINT \"cityKey\" PRIMARY KEY (id)\n)\nWITH (\n\tOIDS = FALSE\n)\nTABLESPACE pg_default;\n"

	cursor.execute(query)
	cursor.execute("ALTER TABLE public.city_table\n\tOWNER to psql_master;\n")


def create_facility_table(conn, cursor, schemas, order):
	schema = schemas["facility_table"]
	query = "CREATE TABLE public.facility_table\n(\n\tid integer NOT NULL DEFAULT nextval(\'facility_table_id_seq\'::regclass),"

	for col_name in order:
		query += "\n\t{name} {t},".format(name=col_name, t=schema[col_name])

	query += "\n\tCONSTRAINT \"facilityKey\" PRIMARY KEY (id),"
	query += "\n\tCONSTRAINT \"cityForeignKey\" FOREIGN KEY (city_id)"
	query += "\n\t\tREFERENCES public.city_table (id) MATCH SIMPLE"
	query += "\n\t\tON UPDATE NO ACTION"
	query += "\n\t\tON DELETE NO ACTION\n)"
	query += "\nWITH (\n\tOIDS = FALSE\n)\nTABLESPACE pg_default;\n"

	cursor.execute(query)
	cursor.execute("ALTER TABLE public.facility_table\n\tOWNER to psql_master;\n")


""" INTERMEDIATE TABLE CREATION """

def create_facility_activity_table(conn, cursor):
	query =  "CREATE TABLE public.facility_activity_table\n("
	query += "\n\tfacility_id integer,"
	query += "\n\tactivity_id integer,"
	query += "\n\tCONSTRAINT \"activKey\" FOREIGN KEY (activity_id)"
	query += "\n\t\tREFERENCES public.activity_table (id) MATCH SIMPLE"
	query += "\n\t\tON UPDATE NO ACTION"
	query += "\n\t\tON DELETE NO ACTION,"
	query += "\n\tCONSTRAINT \"facilityForeignKey\" FOREIGN KEY (facility_id)"
	query += "\n\t\tREFERENCES public.facility_table (id) MATCH SIMPLE"
	query += "\n\t\tON UPDATE NO ACTION"
	query += "\n\t\tON DELETE NO ACTION"
	query += "\n)"
	query += "\nWITH ("
	query += "\n\tOIDS = FALSE"
	query += "\n)"
	query += "\nTABLESPACE pg_default;\n"

	cursor.execute(query)
	cursor.execute("ALTER TABLE public.city_activity_table\n\tOWNER to psql_master;\n")


def create_city_activity_table(conn, cursor):
	query =  "CREATE TABLE public.city_activity_table\n("
	query += "\n\tcity_id integer,"
	query += "\n\tactivity_id integer,"
	query += "\n\tCONSTRAINT \"activityForeignKey\" FOREIGN KEY (activity_id)"
	query += "\n\t\tREFERENCES public.activity_table (id) MATCH SIMPLE"
	query += "\n\t\tON UPDATE NO ACTION"
	query += "\n\t\tON DELETE NO ACTION,"
	query += "\n\tCONSTRAINT \"cityForeignKey\" FOREIGN KEY (city_id)"
	query += "\n\t\tREFERENCES public.city_table (id) MATCH SIMPLE"
	query += "\n\t\tON UPDATE NO ACTION"
	query += "\n\t\tON DELETE NO ACTION"
	query += "\n)"
	query += "\nWITH ("
	query += "\n\tOIDS = FALSE"
	query += "\n)"
	query += "\nTABLESPACE pg_default;\n"

	cursor.execute(query)
	cursor.execute("ALTER TABLE public.city_activity_table\n\tOWNER to psql_master;\n")

##########################################################################################
""" END OF TABLE CREATION CODE """


""" DB POPULATE CODE """

def populate_all_tables(conn, cursor, db_data, schemas):

	# TODO : MAKE THE INSERT STATEMENTS DEPEND ON THE SCHEMAS SO WE CAN CHANGE THE DB SCHEMA
	
	facility_table, activity_table, city_table, city_activity_table, facility_activity_table, city_keys, act_keys = db_data

	city_scratch_table = {}
	facility_scratch_table = {}
	activity_scratch_table = {}


	for key in facility_table:
		fac = facility_table[key]

		py_city_id = fac["city_id"]



		city_record = city_table[py_city_id]
		if py_city_id in city_scratch_table:
			db_city_id = city_scratch_table[py_city_id]
		else:

			cursor.execute("INSERT INTO city_table (City, _state, avg_income, population, num_activities, city_summary, img_url, zip)  \
			VALUES (%s, %s, %s, %s, %s, %s, %s, %s) RETURNING id;", (city_record["City"], city_record["_state"], city_record["avg_income"], city_record["population"], city_record["num_activities"], city_record["city_summary"], city_record["img_url"], city_record["zip"]))
			db_city_id = cursor.fetchone()[0]
			city_scratch_table[py_city_id] = db_city_id




		cursor.execute("INSERT INTO facility_table (facility_name, facility_description, facility_type, directions, phone, email, ada_accessible, img_url, city_id, city_name, cost, lat, _long, address, zip)  \
			VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING id;", (fac["facility_name"], fac["facility_description"], fac["facility_type"], fac["directions"], fac["phone"], fac["email"], \
				fac["ada_accessible"], fac["img_url"], db_city_id, fac["city_name"], fac["cost"], fac["lat"], fac["_long"], fac["address"], fac["zip"]))
		currFacId = cursor.fetchone()[0]

		facility_scratch_table[key] = currFacId

	for key in activity_table: 
		act = activity_table[key]

		cursor.execute("INSERT INTO activity_table (activity, img_url, num_cities, description)  \
			VALUES (%s, %s, %s, %s) RETURNING id;", (act["Activity"], act["img_url"], act["num_cities"], act["description"]))
		db_act_id = cursor.fetchone()[0]
		activity_scratch_table[key] = db_act_id

	for entry in city_activity_table:
		cursor.execute("INSERT INTO city_activity_table (city_id, activity_id)  \
			VALUES (%s, %s);", (city_scratch_table[entry[0]], activity_scratch_table[act_keys[entry[2]]]))


	for entry in facility_activity_table:
		cursor.execute("INSERT INTO facility_activity_table (facility_id, activity_id)  \
			VALUES (%s, %s);", (facility_scratch_table[entry[0]], activity_scratch_table[act_keys[entry[2]]]))



#########################################################################################
""" END OF DB POPULATE CODE """

def table_name_check(table_name, conn):
	global _table_names

	if table_name not in _table_names:
		print("{table_names} is not a valid table name.")
		print("closing database connection.")
		conn.close()
		sys.exit()





if __name__ == "__main__":

	activity_colorder = ["activity", "img_url", "num_cities", "description" ]

	activity_schema = { "activity" : "text COLLATE pg_catalog.\"default\"", \
		"img_url" : "text COLLATE pg_catalog.\"default\"", \
		"num_cities" : "integer", \
		"description" : "text COLLATE pg_catalog.\"default\"" }

	city_colorder = ["city", "_state", "avg_income", "population", "num_activities", "city_summary", "img_url", "zip" ]

	city_schema = { "city" : "text COLLATE pg_catalog.\"default\"",\
		"_state" : "text COLLATE pg_catalog.\"default\"",\
		"avg_income" : "integer",\
		"population" : "integer",\
		"num_activities" : "integer",\
		"city_summary" : "text COLLATE pg_catalog.\"default\"",\
		"img_url" : "text COLLATE pg_catalog.\"default\"",\
		"zip" : "integer" }

	facility_colorder = ["facility_name", "facility_description", "facility_type", "directions", "phone", "email", "ada_accessible", "img_url", "city_id", "city_name", "lat", "_long", "cost", "zip", "address"]

	facility_schema = { "facility_name" : "text COLLATE pg_catalog.\"default\"", \
		"facility_description" : "text COLLATE pg_catalog.\"default\"", \
		"facility_type" : "text COLLATE pg_catalog.\"default\"", \
		"directions" : "text COLLATE pg_catalog.\"default\"", \
		"phone" : "text COLLATE pg_catalog.\"default\"", \
		"email" : "text COLLATE pg_catalog.\"default\"", \
		"ada_accessible" : "text COLLATE pg_catalog.\"default\"", \
		"img_url" : "text COLLATE pg_catalog.\"default\"", \
		"city_id" : "integer", \
		"city_name" : "text COLLATE pg_catalog.\"default\"", \
		"lat" : "text COLLATE pg_catalog.\"default\"", \
		"_long" : "text COLLATE pg_catalog.\"default\"", \
		"cost" : "text COLLATE pg_catalog.\"default\"", \
		"zip" : "integer", \
		"address" : "text COLLATE pg_catalog.\"default\"" }


	table_order = { "facility_table" : facility_colorder, "city_table" : city_colorder, "activity_table" : activity_colorder}
	table_schema = { "facility_table" : facility_schema, "city_table" : city_schema, "activity_table" : activity_schema}


	# parse all command line arguments
	args = parse_args()

	# laod all the pickled data 
	facility_table, activity_table, city_table, city_activity_table, facility_activity_table, city_keys, act_keys = load_everything()

	# divy up cmdln args
	drop_args = args.drop
	create_args = args.create
	pop_args = args.populate
	recreate = args.recreate


	# establish a connection to our remote database
	my_db = Database()
	conn = my_db.conn
	cursor = my_db.cursor


	if recreate.lower() == "true":

		db_data = (facility_table, activity_table, city_table, city_activity_table, facility_activity_table, city_keys, act_keys)

		drop_all_tables(conn, cursor)
		create_all_tables(conn, cursor, table_schema, table_order)
		populate_all_tables(conn, cursor, db_data, table_schema)
		# drop all tables, create all tables, populate all tables

	else:

		# drop stage
		if "none" != drop_args:
			if "all" in drop_args:
				drop_all_tables(conn, cursor, table_schema)
			else:
				for table_name in drop_args:
					
					# check that the table name is valid
					table_name_check(table_name, conn)

					cursor.execute("DROP TABLE public.{name};".format(name=table_name))


					# drop the associated ID sequences if we drop the tables.
					if table_name in {"facility_table", "city_table", "activity_table"}:
						print("Warning: deleting the main instance tables also deletes their id sequence, which may cause foreign key data in other tables to be incorrect.")
						cursor.execute("DROP SEQUENCE public.{name}_id_seq;".format(name=table_name))

					#drop the table

		if "none" != create_args:
			if "all" in create_args:
				create_all_tables(conn, cursor, table_schema, table_order)

			else:
				for table_name in create_args:

					# check that the table name is valid
					table_name_check(table_name, conn)

					# this gross code is because I believe order matters for the SQL
					if table_name == "activity_table":
						create_activity_seq(conn, cursor)
						create_activity_table(conn, cursor, table_schema)
					if table_name == "city_table":
						create_city_seq(conn, cursor)
						create_city_table(conn, cursor, table_schema)
					if table_name == "facility_table":
						create_facility_seq(conn, cursor)
						create_facility_table(conn, cursor, table_schema)
					if table_name == "city_activity_table":
						create_city_activity_table(conn, cursor)
					if table_name == "facility_activity_table":
						create_facility_activity_table(conn, cursor)
					

		if "none" not in pop_args:
			if "all" == pop_args or "all" in pop_args:
				db_data = (facility_table, activity_table, city_table, city_activity_table, facility_activity_table, city_keys, act_keys)

				populate_all_tables(conn, cursor, db_data, table_schema)
			else:
				#for table_name in pop_args:
					
				print("Populating individual tables only is currently not supported.")
				print("To rebuild entire database, run: $ db_populate.py --recreate TRUE")

	conn.commit()
