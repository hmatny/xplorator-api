# pip install psycopg2
import psycopg2
import os
from psycopg2 import sql

class Database:
    def __init__(self):
        self.connect()
        self.table_map = {
            "city" : "city_table",
            "facility" : "facility_table",
            "activity" : "activity_table"
        }
        self.model_name = {
            "city" : "city",
            "facility" : "facility_name",
            "activity" : "activity"
        }
        self.op_map = {
            "less_than" : "<",
            "greater_than" : ">",
            "equal_to" : "="
        }
        self.sort_by_map = {
            "-1" : "desc",
             "1" : "asc"
        }
        
        

    def filter_query_formatter(self, table, attr, predicate):
        query = "SELECT * FROM " + self.table_map[table] + " WHERE ("+ attr + " " + self.op_map[predicate] + " %s) ORDER BY " + attr
        return query

    def search_query_formatter(self, model):
        query = "SELECT * FROM " + self.table_map[model] + " WHERE levenshtein(LOWER(" + self.model_name[model] + "), %s) <= 0;"
        return query
    
    def sort_query_formatter(self, model, attr, sort_by):
        query = "SELECT * FROM " + self.table_map[model] +" order by " +  attr + " " + self.sort_by_map[sort_by] 
        return query

    def connect(self):

        try:
            # print("Trying to make connection to db")
            self.conn = psycopg2.connect(
                database="xplorator_db",
                user="psql_master",
                password="psqlpassword",
                host="psql.cqissclmei0b.us-east-2.rds.amazonaws.com",
                port="5432",
            )
            self.cursor = self.conn.cursor()
        except Exception as e:
            print(e)
        # try:
        #     self.conn = engine = psycopg2.connect(
        #         dbname= os.environ['RDS_DB_NAME'],
        #         user= os.environ['RDS_USERNAME'],
        #         password= os.environ['RDS_PASSWORD'],
        #         host= os.environ['RDS_HOSTNAME'],
        #         port= os.environ['RDS_PORT']
        #     )
        #     # cur = engine.cursor()
        #     # res = cur.execute("select * from city_table")
        #     # print(cur.fetchall())
        #     self.cursor = self.conn.cursor()
        #     return True
        # except Exception as e:
        #     return e

    def is_connected(self):
        if self.conn.closed == 0:
            return True
        return False

    def select_all(self, tablename):
        res = None
        cursor = None
        tablename = self.table_map[tablename]
        try:
            cursor = self.conn.cursor()
            cursor.execute("SELECT * FROM  "+ tablename +" ORDER BY id")
            res = cursor.fetchall()
        except psycopg2.Error as e:
            self.conn.rollback()
        else:
            self.conn.commit()
        return res

    def select_city(self, city_id):
        city_id = (city_id,) #convert city_id to tuple so psycopg unpacks it in executes
        res = []
        city_details = []
        ext_acts = []
        ext_facs = []
        cursor = None
        try:
            cursor = self.conn.cursor()
            # execute
            cursor.execute("SELECT * FROM city_table WHERE id = (%s) ",city_id)

            city_details = cursor.fetchall()
            #gets activity details for the respective city (matching city_id)
            cursor.execute("""
                SELECT act.id, act.activity FROM city_activity_table cat JOIN activity_table act
                ON cat.activity_id = act.id WHERE cat.city_id = (%s) 
                """,city_id)

            ext_acts = cursor.fetchall()
            #gets facility details for the respective city (matching city_id)
            self.cursor.execute("""
                SELECT fac.id, fac.facility_name FROM facility_table fac WHERE fac.city_id = (%s)
                """,city_id)

            ext_facs = cursor.fetchall()
            res.append(city_details)
            res.append(ext_acts)
            res.append(ext_facs)
        except psycopg2.Error as e:
            self.conn.rollback()
        else:
            self.conn.commit()
        return res

    def select_activity(self, act_id):
        act_id = (act_id,) #convert act_id to tuple so psycopg unpacks it in executes
        res = []
        act_details = []
        ext_cities = []
        ext_facs = []

        cursor = None
        try:
            cursor = self.conn.cursor()
            #gets city details from activity_table
            cursor.execute("SELECT * FROM activity_table WHERE id = (%s) ",act_id)
            act_details = cursor.fetchall()

            #gets activity details for the respective city (matching act_id)
            cursor.execute("""
                SELECT city.id, city.city FROM city_activity_table cat JOIN city_table city 
                ON cat.city_id = city.id WHERE cat.activity_id = (%s)
                """,act_id)
            ext_cities = cursor.fetchall()

            #gets facility details for the respective city (matching act_id)
            cursor.execute("""
                SELECT fac.id, fac.facility_name FROM facility_activity_table fat JOIN facility_table
                 fac ON fat.facility_id = fac.id WHERE fat.activity_id = (%s)
                """,act_id)
            ext_facs = cursor.fetchall()
            res.append(act_details)
            res.append(ext_cities)
            res.append(ext_facs)

        except psycopg2.Error as e:
            self.conn.rollback()
        else:
            self.conn.commit()
        
        return res



    def select_facility(self, fac_id):
        fac_id = (fac_id,) #convert fac_id to tuple so psycopg unpacks it in executes
        res = []
        fac_details = []
        ext_city = []
        ext_acts = []

        cursor = None
        try:
            #gets facility details from city_table
            cursor = self.conn.cursor()
            cursor.execute("SELECT * FROM facility_table WHERE id = (%s) ",fac_id)

            fac_details = cursor.fetchall()
            #gets activity details for the respective facility (matching fac_id)
            cursor.execute("""
                SELECT act.id, act.activity FROM facility_activity_table fat JOIN activity_table act
                ON fat.activity_id = act.id WHERE fat.facility_id = (%s)
                """,fac_id)

            ext_acts = cursor.fetchall()
            res.append(fac_details)
            res.append(ext_acts)
            res.append(ext_city)
        except psycopg2.Error as e:
            self.conn.rollback()
        else:
            self.conn.commit()

        return res

    def search_all_formatter(self, model):
        if model == "activity":
            query = "SELECT * FROM " + self.table_map[model] + " WHERE (%s LIKE num_cities::text OR levenshtein(LOWER(activity),%s) <= 0);"
        elif model == "city":
            query = "SELECT * FROM " + self.table_map[model] + " WHERE (levenshtein(LOWER(city),%s) <= 0 or num_activities::text = %s  or levenshtein(round(population,-2)::text,%s) <= 0)"
        else:
            query = "SELECT * FROM " + self.table_map[model] + " WHERE (levenshtein(LOWER(city_name),%s) <= 0 or levenshtein(LOWER(facility_name),%s) <= 0 or levenshtein(LOWER(facility_type),%s) <= 0 or levenshtein(zip::text,%s) <= 0)" 
        return query

    def search_all(self, term):
        # grabs the appropriate queries to execute
        term = term.lower()
        act_query = self.search_all_formatter("activity")
        act_fields = (term, term)
        act_res = []
        fac_query = self.search_all_formatter("facility")
        fac_fields = (term, term , term, term)
        fac_res = []
        cit_query = self.search_all_formatter("city")
        cit_fields = (term, term, term)
        cit_res = []

        res = []

        cursor = None
        try:
            cursor = self.conn.cursor()

            cursor.execute(act_query, act_fields)
            act_res = cursor.fetchall()
          
            cursor.execute(fac_query, fac_fields)
            fac_res = cursor.fetchall()

            cursor.execute(cit_query, cit_fields)
            cit_res = cursor.fetchall()
        except psycopg2.Error as e:
            self.conn.rollback()
        else:
            self.conn.commit()

        res = (cit_res, act_res, fac_res)
        return res

    def search(self, model, name):
        query = self.search_query_formatter(model)
        fields = (name.lower(), )
        cursor = None
        res = {}
        try:
            cursor = self.conn.cursor()
            cursor.execute(query,fields)
            res = cursor.fetchall()
        except psycopg2.Error as e:
            self.conn.rollback()
        else:
            self.conn.commit()
        return res

    def filter(self, model, attr, predicate, value):
        query = self.filter_query_formatter(model, attr, predicate)
        attr = attr
        val = value
        fields = (val,)
        res = "empty"
        cursor = None
        try:
            cursor = self.conn.cursor()
            cursor.execute(query,fields)
            res = cursor.fetchall()
        except psycopg2.Error as e:
            self.conn.rollback()
            
        else:
            self.conn.commit()
        return res

    def sort(self, model, attr, sort_by):
        query = self.sort_query_formatter(model, attr, sort_by)
        cursor = None
        try:
            cursor = self.conn.cursor()
            cursor.execute(query)
            res = cursor.fetchall()
        except psycopg2.Error as e:
            self.conn.rollback()
        else:
            self.conn.commit()
        return res

    