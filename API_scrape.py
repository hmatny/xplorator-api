
import json
import requests
import wikipedia
import sqlalchemy
import pprint
import pickle
import re

from uszipcode import SearchEngine
import psycopg2
import os
from psycopg2 import sql
from db import Database
""" 
Pulls data from the various APIs and combines it in a `format that is easily convertible to database tables
"""

city_keys = {}
act_keys = {}
city_activity_table = set()
city_facility_table = set()
facility_activity_table = set()


"""
Models to populate 

City
-> links to its facilities
-> links to its activities

Activity
-> links to facilities
-> links to cities

Facility
-> links to its city (singular)
-> links to its activities

"""


# example of getting json info from the RIDB database
#response = requests.get("https://ridb.recreation.gov/api/v1/activities", headers={"apikey":"68efa0f3-4115-4602-b2bd-06e1baee0e52"})
		#print(response.content)

# example of using weird income API
#response = requests.get("https://income.p.rapidapi.com/api/income/174.20.20.55", headers={"X-RapidAPI-Key":"b4cb66f424mshe5b70362631520bp1c957ajsn8a4cbf3f9e5b"})
		#print(response.content)

# darksky weather API 
# key 4ed02c10ba822811ff530f66564e5aa5
# schema https://api.darksky.net/forecast/[key]/[latitude],[longitude]
# https://darksky.net/dev/docs
#response = requests.get("https://api.darksky.net/forecast/4ed02c10ba822811ff530f66564e5aa5/42.3601,-71.0589")
	#print(response.content)

# wikimeda/wikipedia API
# https://en.wikipedia.org/api/rest_v1/
#response = requests.get("https://en.wikipedia.org/w/api.php?action=query&titles=San%20Francisco,%20California&prop=revisions&rvprop=content&format=json")
#print(response.content)


# unused, but here's the key 50e44a8f510b1dcde9cbf4a347792491f14172a7
# https://github.com/datamade/census


""" Here is one way to solve the Gregory Hines problem"""
state_abb_to_name = {
        'AK': 'Alaska',
        'AL': 'Alabama',
        'AR': 'Arkansas',
        'AS': 'American Samoa',
        'AZ': 'Arizona',
        'CA': 'California',
        'CO': 'Colorado',
        'CT': 'Connecticut',
        'DC': 'District of Columbia',
        'DE': 'Delaware',
        'FL': 'Florida',
        'GA': 'Georgia',
        'GU': 'Guam',
        'HI': 'Hawaii',
        'IA': 'Iowa',
        'ID': 'Idaho',
        'IL': 'Illinois',
        'IN': 'Indiana',
        'KS': 'Kansas',
        'KY': 'Kentucky',
        'LA': 'Louisiana',
        'MA': 'Massachusetts',
        'MD': 'Maryland',
        'ME': 'Maine',
        'MI': 'Michigan',
        'MN': 'Minnesota',
        'MO': 'Missouri',
        'MP': 'Northern Mariana Islands',
        'MS': 'Mississippi',
        'MT': 'Montana',
        'NA': 'National',
        'NC': 'North Carolina',
        'ND': 'North Dakota',
        'NE': 'Nebraska',
        'NH': 'New Hampshire',
        'NJ': 'New Jersey',
        'NM': 'New Mexico',
        'NV': 'Nevada',
        'NY': 'New York',
        'OH': 'Ohio',
        'OK': 'Oklahoma',
        'OR': 'Oregon',
        'PA': 'Pennsylvania',
        'PR': 'Puerto Rico',
        'RI': 'Rhode Island',
        'SC': 'South Carolina',
        'SD': 'South Dakota',
        'TN': 'Tennessee',
        'TX': 'Texas',
        'UT': 'Utah',
        'VA': 'Virginia',
        'VI': 'Virgin Islands',
        'VT': 'Vermont',
        'WA': 'Washington',
        'WI': 'Wisconsin',
        'WV': 'West Virginia',
        'WY': 'Wyoming'
}


tag_regex = re.compile(r'<[^>]+>')


def remove_html(string):
	return tag_regex.sub('', string)



# ununsed
def getBestImage(images, keyword=None):
	if keyword != None:
		for image_url in images:
			if keyword.lower().replace(" ", "_") in image_url.lower():
		 		return image_url
	for image_url in images:
		if ".svg" not in image_url:
			return image_url

	return images[0]


def makeCityData():
	global city_keys
	global city_activity_table
	global city_facility_table

	search = SearchEngine()

	allCitiesData = {}
	for city_state in city_keys:

		city_id = city_keys[city_state]
		#print(city_id)

		# iterate over the city-activity relationship table and count the number of
		# activities that are availible in this city
		num_activities = 0
		for entry in city_activity_table:
			if entry[0] == city_id:
				num_activities += 1

		#print("*********** " + city_state[0])

		cityData = {}
		cityData["City"] = city_state[0]
		cityData["_state"] = city_state[1]

		uszipcode_data = search.by_city_and_state( city_state[0], city_state[1] )


		# Here we save the actual name of the state
		state_longname = state_abb_to_name[city_state[1]]



		if len(uszipcode_data) != 0:
			city_info = uszipcode_data[0].to_dict()

			cityData["population"] = city_info["population"]
			cityData["zip"] = city_info["zipcode"]
			cityData["avg_income"] = city_info["median_household_income"]
		else:
			cityData["population"] = 0
			cityData["zip"] = 0
			cityData["avg_income"] = 0



		cityData["num_activities"] = num_activities


		#print(city_state[0] + ", " + city_state[1])
		try:
			pgname = wikipedia.search(city_state[0] + ", " + state_longname)[0]
		except Exception as e:
			cityData["city_summary"] = "No summary available at this time."
			cityData["img_url"] = "https://image.flaticon.com/icons/png/512/119/119583.png"
		else:
			cityData["city_summary"] = wikipedia.summary(city_state[0] + ", " + state_longname, sentences=3)

			cityData["img_url"] = json.loads(requests.get("https://en.wikipedia.org/api/rest_v1/page/summary/" + pgname).text)["originalimage"]["source"]

			


		

		allCitiesData[city_id] = cityData

	return allCitiesData




def makeActivityData():

	global city_keys
	global city_activity_table
	global city_facility_table
	global act_keys

	primaryKey = 0
	allActivitiesData = {}
	
	
	response = requests.get("https://ridb.recreation.gov/api/v1/activities?offset=0", headers={"apikey":"68efa0f3-4115-4602-b2bd-06e1baee0e52"})
	allActivitiesJson = json.loads(response.text)
	

	i = 0
	for activityJson in allActivitiesJson["RECDATA"]:
		#print("inside parent for1")
		#print(i)

		activityData = {}
		activityData["Activity"] = activityJson["ActivityName"]

		num_cities = 0
		for entry in city_activity_table:
			#print("inside baby for1")
			if entry[2] == activityJson["ActivityName"]:
				num_cities += 1
		
		activityData["img_url"] = ""
		activityData["description"] = ""
		
		try:
			pgname = wikipedia.search(activityJson["ActivityName"])[0]
			activityData["img_url"] = json.loads(requests.get("https://en.wikipedia.org/api/rest_v1/page/summary/" + pgname).text)["originalimage"]["source"]
			activityData["description"] = wikipedia.summary(activityJson["ActivityName"], sentences=2)
		except:
			activityData["img_url"] = "default"
		activityData["num_cities"] = num_cities
		


		allActivitiesData[primaryKey] = activityData
		act_keys[activityData["Activity"]] = primaryKey
		primaryKey += 1
		i += 1

	response = requests.get("https://ridb.recreation.gov/api/v1/activities?offset=50", headers={"apikey":"68efa0f3-4115-4602-b2bd-06e1baee0e52"})
	allActivitiesJson = json.loads(response.text)
	


	for activityJson in allActivitiesJson["RECDATA"]:


		activityData = {}
		activityData["Activity"] = activityJson["ActivityName"]

		num_cities = 0
		for entry in city_activity_table:
			if entry[2] == activityJson["ActivityName"]:
				num_cities += 1
		
		activityData["img_url"] = ""
		activityData["description"] = ""
		
		try:
			pgname = wikipedia.search(activityJson["ActivityName"])[0]
			activityData["img_url"] = json.loads(requests.get("https://en.wikipedia.org/api/rest_v1/page/summary/" + pgname).text)["originalimage"]["source"]
			activityData["description"] = wikipedia.summary(activityJson["ActivityName"], sentences=2)
		except:
			activityData["img_url"] = "default"
		activityData["num_cities"] = num_cities
		


		allActivitiesData[primaryKey] = activityData
		act_keys[activityData["Activity"]] = primaryKey
		primaryKey += 1

	response = requests.get("https://ridb.recreation.gov/api/v1/activities?offset=100", headers={"apikey":"68efa0f3-4115-4602-b2bd-06e1baee0e52"})
	allActivitiesJson = json.loads(response.text)
	


	for activityJson in allActivitiesJson["RECDATA"]:


		activityData = {}
		activityData["Activity"] = activityJson["ActivityName"]

		num_cities = 0
		for entry in city_activity_table:
			if entry[2] == activityJson["ActivityName"]:
				num_cities += 1
		
		activityData["img_url"] = ""
		activityData["description"] = ""
		
		try:
			pgname = wikipedia.search(activityJson["ActivityName"])[0]
			activityData["img_url"] = json.loads(requests.get("https://en.wikipedia.org/api/rest_v1/page/summary/" + pgname).text)["originalimage"]["source"]
			activityData["description"] = wikipedia.summary(activityJson["ActivityName"], sentences=2)
		except:
			activityData["img_url"] = "default"
		activityData["num_cities"] = num_cities
		


		allActivitiesData[primaryKey] = activityData
		act_keys[activityData["Activity"]] = primaryKey
		primaryKey += 1


	return allActivitiesData


def makeFacilityData(num_to_get=200):

	global city_keys
	global city_activity_table
	global city_facility_table
	global facility_activity_table

	step_size = 50

	allFacilitiesData = {}

	# keeping track of facility and city indices because we get data for both from this API call
	# names are confusing but "primaryKey" is facility and "cityKey" is cityKey. 
	primaryKey = 0
	cityKey = 0

	for offset in range(0, num_to_get, step_size):


		response = requests.get("https://ridb.recreation.gov/api/v1/facilities?limit=50&offset={offset}&full=true".format(offset=offset), verify=False, headers={"apikey":"68efa0f3-4115-4602-b2bd-06e1baee0e52"})
		
		# this is the JSON data for the next 50 facilities
		allFacilitiesJson = json.loads(response.text)

		# go over each individual facility in this response body
		for facilityJson in allFacilitiesJson["RECDATA"]:

			# set imageURL to default image for facilities, then replace if we find an image
			image_url = "https://image.flaticon.com/icons/png/512/433/433102.png"
			for entry in facilityJson["MEDIA"]:
				if entry["MediaType"] == "Image":
					 image_url = entry["URL"]
					 break

			facilityData = {}
			facilityData["facility_name"] = facilityJson["FacilityName"]

			# We noticed that sometimes these descriptions include HTML tags so we want to get rid of those before we store them
			facilityData["facility_description"] = remove_html(facilityJson["FacilityDescription"])
			facilityData["facility_type"] = facilityJson["FacilityTypeDescription"]
			facilityData["directions"] = facilityJson["FacilityDirections"]
			facilityData["phone"] = facilityJson["FacilityPhone"]
			facilityData["email"] = facilityJson["FacilityEmail"]
			facilityData["ada_accessible"] = facilityJson["FacilityAdaAccess"]
			facilityData["img_url"] = image_url
			
			
			facilityData["cost"] = facilityJson["FacilityUseFeeDescription"]
			facilityData["lat"] = facilityJson["FacilityLatitude"]
			facilityData["_long"] = facilityJson["FacilityLongitude"]


			# exclude facilities with no address, because then we can't get their state code
			if len(facilityJson["FACILITYADDRESS"]) == 0:
				continue

			addressString = facilityJson["FACILITYADDRESS"][0]["FacilityStreetAddress1"] + " " + facilityJson["FACILITYADDRESS"][0]["FacilityStreetAddress2"] + " " + facilityJson["FACILITYADDRESS"][0]["FacilityStreetAddress3"]
			facilityData["address"] = addressString

			zipcode = facilityJson["FACILITYADDRESS"][0]["PostalCode"]
			if zipcode == "":
				zipcode = 0
			else:
				zipcode = int(zipcode.replace("-",""))

			facilityData["zip"] = zipcode
			facilityData["city_name"] = facilityJson["FACILITYADDRESS"][0]["City"]

			# update tables

			city = facilityData["city_name"].title()
			state = facilityJson["FACILITYADDRESS"][0]["AddressStateCode"]

			# for now, exclude facilities with no listed city
			if city == "":
				continue

			# add this (city_state) combination to our records if it doesn't exist already
			if (city, state) in city_keys:
				city_id = city_keys[(city, state)]
			else:
				city_id = cityKey
				city_keys[(city, state)] = city_id
				cityKey += 1 


			facilityData["city_id"] = city_id

			for activity in facilityJson["ACTIVITY"]:
				city_activity_table.add( (city_id, city, activity["ActivityName"]) )
				facility_activity_table.add( (primaryKey, facilityData["facility_name"], activity["ActivityName"] ) )

			city_facility_table.add( (city_id, city, facilityData["facility_name"], primaryKey) )

			allFacilitiesData[primaryKey] = facilityData

			primaryKey += 1


	return allFacilitiesData


def store_everything(t1, t2, t3, t4, t5, t6, t7):
	with open("{name}.pickle".format(name="t1"), "wb") as handle:
		pickle.dump(t1, handle, protocol=pickle.HIGHEST_PROTOCOL)

	with open("{name}.pickle".format(name="t2"), "wb") as handle:
		pickle.dump(t2, handle, protocol=pickle.HIGHEST_PROTOCOL)

	with open("{name}.pickle".format(name="t3"), "wb") as handle:
		pickle.dump(t3, handle, protocol=pickle.HIGHEST_PROTOCOL)

	with open("{name}.pickle".format(name="t4"), "wb") as handle:
		pickle.dump(t4, handle, protocol=pickle.HIGHEST_PROTOCOL)

	with open("{name}.pickle".format(name="t5"), "wb") as handle:
		pickle.dump(t5, handle, protocol=pickle.HIGHEST_PROTOCOL)

	with open("{name}.pickle".format(name="t6"), "wb") as handle:
		pickle.dump(t6, handle, protocol=pickle.HIGHEST_PROTOCOL)

	with open("{name}.pickle".format(name="t7"), "wb") as handle:
		pickle.dump(t7, handle, protocol=pickle.HIGHEST_PROTOCOL)

def load_everything():

	result = tuple()

	for n in ["t1", "t2", "t3", "t4", "t5", "t6", "t7"]:
		with open("{name}.pickle".format(name=n), "rb") as handle:
			result += ( pickle.load(handle), )
	return result

def do_everything():
	global city_keys
	global city_activity_table
	global city_facility_table # don't use
	global facility_activity_table
	global act_keys

	facility_table = makeFacilityData()

	activity_table = makeActivityData()

	city_table = makeCityData()




	pp = pprint.PrettyPrinter(indent=4)
	pp.pprint(facility_table)
	pp.pprint(activity_table)
	pp.pprint(city_table)


	store_everything(facility_table, activity_table, city_table, city_activity_table, facility_activity_table, city_keys, act_keys)



if __name__== "__main__":
	do_everything()

