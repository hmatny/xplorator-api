FROM tiangolo/meinheld-gunicorn-flask:python3.7

# Copy the app into the image
COPY . .

# Copy the requirements.txt into the image and install the requirements
#COPY requirements.txt .
RUN pip install --upgrade pip
RUN pip install -r ./requirements.txt

CMD [ "python", "./application.py" ]
