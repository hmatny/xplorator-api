from unittest import main, TestCase
import json
import os
from flask import Flask, Response
from model import ModelBuilder

class tests(TestCase):
        
    def setUp(self):
        self.app = Flask(__name__)
        self.city_data = ["Austin", "City", "image source", "This is a city", "Texas", "href", 
                     "45000", "5 million people", "3 activities", "78257"]
        self.model = "city"
        self.activity_data = [[[1, 'Testing', 'www.imageofzach.com', 3, 'This is a fun test activity call testing']], 
                              ['Austin', 'Seattle', 'San Diego'], ['Yosimite Campground', 'RV park', 'turtle beach']]
        self.facility_data = [[[1, 'Yosimite CampGround', 'A place to camp on the ground', 'Campground', 'Up your butt and around the corner', 
                              '867-5309', 'therealdonaldtrump@gmail.com', 'Y', 'www.imageofhadi.com', 1, 'Austin', '38.23', '29.23', 'about 3.50', 
                              '78705', '1 University Way, Austin, Texas']], 
                              ['Testing Code', 'Front End Development', 'Back End Development'], ['fake', 'shouldnt show up', 'it gets city data from facility and ther is only 1']]
        self.city_data_full = [[[1, 'Austin', 'Texas', '54000', '900000', '16', 'The best city in the United States 3 years running', 'www.imageofaustin.com', '78705']], 
                               ['Testing Code', 'Testing Front-End', 'Testing Back-End'], ['Turtle Pond', 'Ut Tower', 'SoDoSoPa']]
        self.city_data_array = [[[[1, 'Austin', 'Texas', '54000', '900000', '16', 'The best city in the United States 3 years running', 'www.imageofaustin.com', '78705'], 
                               ['Testing Code', 'Testing Front-End', 'Testing Back-End'], ['Turtle Pond', 'Ut Tower', 'SoDoSoPa']],
                               [[2, 'Seattle', 'Washington', '70,000', '1.3 million', '11', 'Like austin but just in washington', 'www.imageofseattle.com', '12345'], 
                               ['Testing Code', 'Testing Front-End', 'Testing Back-End'], ['Amazon Bldg', 'Amazon Workers', 'Amazon']],
                               [[3, 'San Diego', 'California', '60000', '1.1 million', '9', 'Most beatiful city ever', 'www.imageofsandiego.com', '54321'], 
                               ['Testing Code', 'Testing Front-End', 'Testing Back-End'], ['Sun', 'Great Weather', 'Beaches']]]]

    #lets test the activity instance function 
    def test_build_activity_instance(self):
        #must pass modelbuilder() so that it can access those "self" functions during the test
        activity = ModelBuilder.build_activity_instance(ModelBuilder(), self.activity_data)
        #must load activity from raw string to actual json dict
        ajson = json.loads(activity)
        #compare to json
        self.assertEqual(ajson,  {'instance-name': 'Testing', 'instance-description': 'This is a fun test activity call testing', 
            'img-src': 'www.imageofzach.com', 'href': 'activity/1', 'attributes': 
            {'displayAttributes': {'num-cities': 3}, 'otherAttributes': {'num-cities': 3}}, 'external-models': 
            {'Cities': {'u': '/city/A', 'e': '/city/S', 'a': '/city/S'}, 
            'Facilities': {'o': '/facility/Y', 'V': '/facility/R', 'u': '/facility/t'}}})

 
    #lets test the facility instance function
    def test_build_facility_instance(self):
        #pass in ModelBuilder() so it can call the correct ModelBuilder functions
        facility = ModelBuilder.build_facility_instance(ModelBuilder(), self.facility_data)
        #load the json from raw string
        fjson = json.loads(facility)
        self.assertEqual(fjson, {'instance-name': 'Yosimite CampGround', 'instance-description': 'A place to camp on the ground',
         'img-src': 'www.imageofhadi.com', 'href': 'facility/1', 'attributes': {'displayAttributes': {'Facility-Type': 'Campground',
          'phone': '867-5309', 'email': 'therealdonaldtrump@gmail.com', 'Zip': '78705'}, 'otherAttributes': {'Directions': 
          'Up your butt and around the corner', 'ADA-Accessible': 'Y', 'City-Name': 'Austin', 'Lattitude': '38.23', 'Longitude': '29.23',
           'Cost': 'about 3.50', 'Address': '1 University Way, Austin, Texas', 'phone': '867-5309', 'email': 'therealdonaldtrump@gmail.com',
            'Zip': '78705'}}, 'external-models': {'Activities': {'e': '/activity/T', 'r': '/activity/F', 'a': '/activity/B'}, 
            'Cities': {'Austin': '/city/1'}}})

    #lets test the city instance function
    def test_build_city_instance(self):
        city = ModelBuilder.build_city_instance(ModelBuilder(), self.city_data_full)
        cjson = json.loads(city)
        #cjson['attributes']['otherAttributes']["Average_Income"] = '54000'
        self.assertEqual(cjson, {'instance-name': 'Austin, Texas', 'instance-description': 
                                'The best city in the United States 3 years running', 'img-src': 'www.imageofaustin.com',
                                 'href': '/city/1', 'attributes': {'otherAttributes': {'Average_Income': '54000', 'Zipcode': '78705', 'Population': '900000'},
                                  'displayAttributes': {'Population': '900000', 'Num_Activities': '16', 'Average_Income': '54000'}}, 
                                  'external-models': {'Activities': {'e': '/activity/T'}, 
                                  'Facilities': {'u': '/facility/T', 't': '/facility/U', 'o': '/facility/S'}}})

    #lets test build_all with an array of cities
    def test_build_all(self):
        model_builder = ModelBuilder()
        all = ModelBuilder.build_all(model_builder, self.city_data_array, model_builder.build_city_instance,len(self.city_data_array))
        ajson = json.loads(all)
        # ajson["instance-data"]["[1, 'Austin', 'Texas', '54000', '900000', '16', 'The best city in the United States 3 years running', 'www.imageofaustin.com', '78705']"]["Average_Income"] = '54000'
        # ajson["instance-data"]["[2, 'Seattle', 'Washington', '70,000', '1.3 million', '11', 'Like austin but just in washington', 'www.imageofseattle.com', '12345']"]["Average_Income"] = '70,000'
        # ajson["instance-data"]["[3, 'San Diego', 'California', '60000', '1.1 million', '9', 'Most beatiful city ever', 'www.imageofsandiego.com', '54321']"]["Average_Income"] = '60000'

        #I passed in 3 cities but the count is 4, off by 1
        self.assertEqual(ajson["total-count"], len(self.city_data_array))        


    # tests that methods return non-none values
    def test_build_city(self):
        build_city = ModelBuilder.build_city_details(self, self.city_data)
        self.assertIsNotNone(build_city)

    # def test_build_instance_data(self):
    #     build_instance_data = ModelBuilder.build_instance_data(self, self.model, self.city_data)
    #     self.assertIsNotNone(build_instance_data)

    def test_build_instance_data_2(self):
        build_instance_data = ModelBuilder.build_instance_data(self, self.model, self.city_data)
        self.assertIsInstance(build_instance_data, dict)

    # tests that content is as expected 
    def test_build_instance_data_3(self):
        build_instance_data = ModelBuilder.build_instance_data(self, self.model, self.city_data)
        self.assertEqual(build_instance_data["instance-name"], "City")

    def tearDown(self):
        print()

if __name__ == "__main__":
    main()
